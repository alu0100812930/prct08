require 'spec_helper'

describe DoublyLinkedList do 
    before:each do 
        @libro = Book.new(:author => ["Dave Thomas", "Andy Hunt", "Chad Fowler"], :title => "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide", :series => "The Facets of Ruby", :p_house => "Pragmatic Bookshelf",:edit_num => 4, :p_date => "July 7, 2013", :isbn_num => ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"])
        @revista= Magazine.new(:author => "CASTAGNINO, Juan M", :title_a => "Técnicas, materiales y aplicaciones en nanotecnología", :title_m => "Acta Bioquímica Clínica Latinoamericana", :volume => "41", :edit_num => 2, :pages => "189-191", :p_date => "abril / junio 2007")
        @diario= Newspaper.new(:author => "AGUAD, Juan", :title_a => "Más cerca", :title_p => "El Mercurio", :p_place => "Santiago", :p_date => "16 de enero de 2008", :pages => "p. 4, Suplemento Deportes", :column => 1)
        @edoc= EDoc.new(:author => ["Scott Chacon", "Ben Straub"], :p_date=> 2009, :title => "Pro Git 2009th Edition", :URL => "https://git-scm.com/book/en/v2")
        @listad= DList.new
    end
    
          
    
    context "Jerarquía de clases" do
      describe "Las clases hijas son hijas de la clase padre Biblioref" do
 describe Book do
         it {expect(described_class).to be < Biblioref}
          it "Un objeto de la clase Book tiene un autor o grupo de autores, un título, una serie (opcional), una editorial, un número de edición, una fecha de publicación y uno o más números ISBN" do
    expect(@libro.author).to be == ["Dave Thomas", "Andy Hunt", "Chad Fowler"]
    expect(@libro.title).to be == "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide"
    expect(@libro.series).to be == "The Facets of Ruby"
    expect(@libro.p_house).to be == "Pragmatic Bookshelf"
    expect(@libro.edit_num).to be == 4
    expect(@libro.p_date).to be == "July 7, 2013"
    expect(@libro.isbn_num).to be == ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"]
    end
    it "Un objeto de la clase Book tiene una salida formateada especial" do
        expect(@libro.to_s).to be == "Dave Thomas, Andy Hunt, Chad Fowler. Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide, (The Facets of Ruby), Pragmatic Bookshelf; 4 edition (July 7, 2013), ISBN-13: 978-1937785499, ISBN-10: 1937785491."
    end
end
describe Magazine do
         it{expect(described_class).to be < Biblioref}
         it "Un objeto de la clase Magazine tiene un autor o grupo de autores, un título de artículo, un título de revista, un volumen, un número de edición, una paginación y una fecha de publicación." do
         expect(@revista.author).to be == ["CASTAGNINO, Juan M"]
         expect(@revista.title_a).to be == "Técnicas, materiales y aplicaciones en nanotecnología"
         expect(@revista.title_m).to be == "Acta Bioquímica Clínica Latinoamericana"
         expect(@revista.volume).to be == "41"
         expect(@revista.edit_num).to be == 2
         expect(@revista.pages).to be == "189-191"
         expect(@revista.p_date).to be == "abril / junio 2007"
         end
         it "Un objeto de la clase Magazine tiene una salida formateada especial" do
        expect(@revista.to_s).to be == "CASTAGNINO, Juan M. Técnicas, materiales y aplicaciones en nanotecnología. Acta Bioquímica Clínica Latinoamericana. 41 (2): 189-191, (abril / junio 2007)."
    end
end
describe Newspaper do
         it{expect(described_class).to be < Biblioref}
          it "Un objeto de la clase Newspaper tiene un autor o grupo de autores, un título de artículo, un título de periódico, un volumen, un número de edición, una paginación y una fecha de publicación." do
         expect(@diario.author).to be == ["AGUAD, Juan"]
         expect(@diario.title_a).to be == "Más cerca"
         expect(@diario.title_p).to be == "El Mercurio"
         expect(@diario.p_place).to be == "Santiago"
         expect(@diario.pages).to be == "p. 4, Suplemento Deportes"
         expect(@diario.p_date).to be == "16 de enero de 2008"
          expect(@diario.column).to be == 1
         end
         it "Un objeto de la clase Newspaper  tiene una salida formateada especial" do
             expect(@diario.to_s).to be == "AGUAD, Juan. Más cerca. El Mercurio, Santiago, 16 de enero de 2008, p. 4, Suplemento Deportes, col. 1."
         end
end
describe EDoc do
         it{expect(described_class).to be < Biblioref}
          it "Un objeto de la clase Book tiene un autor o grupo de autores, una fecha de publicación, un título y una dirección URL" do
              expect(@edoc.author).to be == ["Scott Chacon", "Ben Straub"]
              expect(@edoc.p_date).to be == 2009
              expect(@edoc.title).to be == "Pro Git 2009th Edition"
              expect(@edoc.url).to be == "https://git-scm.com/book/en/v2"
          end
          it "Un objeto de la clase EDoc tiene una salida formateada especial" do
             expect(@edoc.to_s).to be == "Scott Chacon, Ben Straub. 2009. Pro Git 2009th Edition. Available on: https://git-scm.com/book/en/v2 ."
         end
end
end
        
        it "Cada objeto de las clases hijas se inicializan éxitosamente con unos parámetros y retornan sus respectivos objetos" do
            expect(@libro).to be_a Book 
            expect(@revista).to be_a Magazine
            expect(@diario).to be_a Newspaper
            expect(@edoc).to be_a EDoc
        end
        
        
    end
    
    context "Lista doblemente enlazada" do
        
        it "Toma o no un objeto  y retorna un objeto de clase DList" do
    expect(@listad).to be_a DList
    expect(DList.new(@libro)).to be_a DList
  end
  
  it "Puede haber una lista vacía" do
    expect(@listad.head).to be == nil
    expect(@listad.end_).to be == nil
  end
  
  it "Toma un primer objeto y lo guarda como un nodo de inicio/head y final de la lista" do
        @listad.insert_h(@libro)
    expect(@listad.head["value"]).to be @libro
    expect(@listad.end_["value"]).to be @libro
    expect(@listad.head["next"]).to be nil
    expect(@listad.head["before"]).to be nil
    expect(@listad.end_["before"]).to be nil
  end
  
  it "Se pueden insertar varios objetos a la vez como nuevos nodos al inicio de la lista" do
  @listad.insert_h([@libro, @revista, @diario])
  expect(@listad.head["value"]).to be @diario
  expect(@listad.head["next"]["value"]).to be @revista
  expect(@listad.head["next"]["next"]["value"]).to be @libro
  expect(@listad.head["next"]["next"]["before"]["value"]).to be @revista
  expect(@listad.head["next"]["before"]["value"]).to be @diario
  expect(@listad.head["before"]).to be == nil
  
end
  
  it "Se pueden insertar varios objetos a la vez como nuevos nodos al final de la lista" do
      @listad.insert_e([@libro, @revista, @diario])
  expect(@listad.head["value"]).to be @libro
  expect(@listad.head["next"]["value"]).to be @revista
  expect(@listad.head["next"]["next"]["value"]).to be @diario
  expect(@listad.head["next"]["next"]["before"]["value"]).to be @revista
  expect(@listad.head["next"]["before"]["value"]).to be @libro
  expect(@listad.head["before"]).to be == nil
end

 it "Se pueden extraer los nodos inicial y final de la lista" do
     @listad.insert_e([@libro, @revista, @diario])
  expect(@listad.extract_head).to be @libro
  expect(@listad.extract_end).to be @diario
  expect(@listad.head["next"]).to be nil
  expect(@listad.head["before"]).to be nil
  expect(@listad.end_["next"]).to be nil
  expect(@listad.end_["before"]).to be nil
end

it "Se puede imprimir la lista normalmente o en reversa" do
    @listad.insert_e([@libro, @revista, @diario, @edoc])
    expect(@listad.to_s).to be == "[#{@libro}]<-->[#{@revista}]<-->[#{@diario}]<-->[#{@edoc}]"
    expect(@listad.reverse).to be == "[#{@edoc}]<-->[#{@diario}]<-->[#{@revista}]<-->[#{@libro}]"
end

    end
    
    
   
    
    
end
