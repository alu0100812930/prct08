Lenguajes y Paradigmas de Programación
==================


Práctica 8 - POO: Herencia
-----------

Autor:

* Mauricio Orta

Descripción
----------------------

Este repositorio contiene un árbol de directorios y ficheros creados con la herramienta Bundler, la cual organiza la estructura necesaria para 
poder obtener una "gema" o librería de ruby.

Se incluyen dos ficheros con clases en lib/doubly_linked_list. El primero es referencia.rb, el cual consta de una clase padre Biblioref que representa a referencias bibliográficas
y clases hijas Book, Magazine, Newspaper y EDoc que la heredan, las cuales representan referencias de libros, artículos de revista, artículos de periódico y documentos
electrónicos respectivamente.

El segundo fichero de clase es doubly_list, el contiene la clase DList, la cual como sugiere el nombre de su fichero posee el comportamiento de
una lista doblemente enlazada, con un enlace al nodo anterior además del siguiente para cada nodo de la lista.

Finalmente, existe un fichero de expectativas doubly_linked_list_spec.rb en el directorio spec, el cual contiene grupos de ejemplos de expectativas
que se fueron escribiendo a medida que se desarrollaba el código para verificar y evaluar su funcionamiento. También se instaló y configuró la herramienta
Guard dentro del proyecto, la cual permite ejecutar automáticamente los ficheros RSpec siempre que se modifican los mismos o los ficheros con el código
de las clases que prueban en sus expectativas.


Árbol de ficheros y directorios
-------------------------------
``` 
.
├── CODE_OF_CONDUCT.md
├── Gemfile
├── Gemfile.lock
├── Guardfile
├── LICENSE.txt
├── README.md
├── Rakefile
├── bin
│   ├── console
│   └── setup
├── doubly_linked_list.gemspec
├── lib
│   ├── doubly_linked_list
│   │   ├── doubly_list.rb
│   │   ├── referencia.rb
│   │   └── version.rb
│   └── doubly_linked_list.rb
└── spec
    ├── doubly_linked_list_spec.rb
    └── spec_helper.rb

``` 
    
---------------------------
